-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versi server:                 5.7.16-log - MySQL Community Server (GPL)
-- OS Server:                    Win32
-- HeidiSQL Versi:               9.4.0.5165
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Membuang struktur basisdata untuk spmp
CREATE DATABASE IF NOT EXISTS `spmp` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `spmp`;

-- membuang struktur untuk table spmp.admin
CREATE TABLE IF NOT EXISTS `admin` (
  `id_admin` int(11) NOT NULL AUTO_INCREMENT,
  `nm_admin` varchar(50) DEFAULT '0',
  `username` varchar(50) DEFAULT '0',
  `password` varchar(50) DEFAULT '0',
  PRIMARY KEY (`id_admin`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Membuang data untuk tabel spmp.admin: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
REPLACE INTO `admin` (`id_admin`, `nm_admin`, `username`, `password`) VALUES
	(1, 'Administrator', 'administrator', '21232f297a57a5a743894a0e4a801fc3');
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;

-- membuang struktur untuk table spmp.gejala
CREATE TABLE IF NOT EXISTS `gejala` (
  `kd_gejala` int(11) NOT NULL AUTO_INCREMENT,
  `nm_gejala` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`kd_gejala`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Membuang data untuk tabel spmp.gejala: ~1 rows (lebih kurang)
/*!40000 ALTER TABLE `gejala` DISABLE KEYS */;
REPLACE INTO `gejala` (`kd_gejala`, `nm_gejala`) VALUES
	(1, 'suara serak 2');
/*!40000 ALTER TABLE `gejala` ENABLE KEYS */;

-- membuang struktur untuk table spmp.kerusakan
CREATE TABLE IF NOT EXISTS `kerusakan` (
  `kd_kerusakan` int(11) NOT NULL AUTO_INCREMENT,
  `nm_kerusakan` varchar(100) DEFAULT NULL,
  `solusi` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`kd_kerusakan`),
  KEY `kd_kerusakan` (`kd_kerusakan`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Membuang data untuk tabel spmp.kerusakan: ~2 rows (lebih kurang)
/*!40000 ALTER TABLE `kerusakan` DISABLE KEYS */;
REPLACE INTO `kerusakan` (`kd_kerusakan`, `nm_kerusakan`, `solusi`) VALUES
	(2, '123', '1dwadawd\r\n');
/*!40000 ALTER TABLE `kerusakan` ENABLE KEYS */;

-- membuang struktur untuk table spmp.rules
CREATE TABLE IF NOT EXISTS `rules` (
  `kd_gejala` varchar(10) DEFAULT NULL,
  `kd_kerusakan` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Membuang data untuk tabel spmp.rules: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `rules` DISABLE KEYS */;
/*!40000 ALTER TABLE `rules` ENABLE KEYS */;

-- membuang struktur untuk table spmp.user
CREATE TABLE IF NOT EXISTS `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `nm_user` varchar(50) NOT NULL DEFAULT '0',
  `username` varchar(50) NOT NULL DEFAULT '0',
  `password` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Membuang data untuk tabel spmp.user: ~2 rows (lebih kurang)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
REPLACE INTO `user` (`id_user`, `nm_user`, `username`, `password`) VALUES
	(1, 'test 123', 'test', '21232f297a57a5a743894a0e4a801fc3'),
	(2, '123', '123', '202cb962ac59075b964b07152d234b70'),
	(3, 'admin', 'admin', '21232f297a57a5a743894a0e4a801fc3');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
