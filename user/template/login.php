<form method="POST">
  <div class="form-group">
    <label>Username</label>
    <input name="username" type="text" class="form-control" placeholder="Username Anda" required>
  </div>
  <div class="form-group">
    <label>Password</label>
    <input name="password" type="password" class="form-control" placeholder="Password Anda" required>
  </div>
  <button name="login" type="submit" class="btn btn-dark">Login</button>
</form>