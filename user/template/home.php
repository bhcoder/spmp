<div class="home">
	<div class="container">
		<div class="row align-items-center">
			<div class="col-8">
				<img src="http://www.clker.com/cliparts/P/N/K/E/2/m/teal-car-md.png" alt="" width="80px">
				<h1>SISTEM PAKAR <br> DIAGNOSA KERUSAKAN <br> MESIN PARKIR PT. SAP</h1>
			</div>
			<div class="col-4">
				<div class="card" style="width: 18rem;">
					<div class="card-body">
						<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
							<li class="nav-item">
								<a class="nav-link active" id="pills-login-tab" data-toggle="pill" href="#login" role="tab" aria-controls="pills-login" aria-selected="true">Login</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" id="pills-register-tab" data-toggle="pill" href="#register" role="tab" aria-controls="pills-register" aria-selected="false">Register</a>
							</li>
						</ul>
						<div class="tab-content" id="pills-tabContent">
							<div class="tab-pane fade show active" id="login" role="tabpanel" aria-labelledby="pills-login-tab">
								
								<?php include "login.php";?>
							</div>
							<div class="tab-pane fade" id="register" role="tabpanel" aria-labelledby="pills-register-tab">
								<?php include "register.php";?>
							</div>
						</div>
					</div>
				</div>
			</div>		
		</div>
	</div>
</div>

<div class="container pt-4 pb-4">
	<h3 class="mb-3">Riwayat Diagnosa Kerusakan</h3>
	<div class="row">
		<?php
		for ($i=0; $i < 6; $i++) { 
		?>
			<div class="col-4">
				<div class="card mb-2">
					<div class="card-body">
						<a href="#" class="font-weight-bold m-0">User: Bugi Hermansyah</a> <br>
						8 Januari 2018
					</div>
				</div>
			</div>
		<?php
		}
		?>
	</div>
</div>