<form method="POST">
  <div class="form-group">
    <label>Nama</label>
    <input name="nm_user" type="text" class="form-control" placeholder="Nama Lengkap Anda" required>
  </div>
  <div class="form-group">
    <label>Username</label>
    <input name="username" type="text" class="form-control" placeholder="Username Anda" required>
  </div>
  <div class="form-group">
    <label>Password</label>
    <input name="password" type="password" class="form-control" placeholder="Password Anda" required>
  </div>
  <button name="register" type="submit" class="btn btn-dark">Register</button>
</form>