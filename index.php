<?php
ob_start();
session_start();
require_once 'lib/koneksi.php';

function getHeader() {
    require_once __DIR__.'/user/template/layout/header.php';
}

function getFooter() {
    require_once __DIR__.'/user/template/layout/footer.php';
}

function sessionUser() {
    if (!isset($_SESSION['id_user'])) {
        header('location:index.php');
    }
}

function alert($message) {
    return '<script>window.alert('.$message.')</script>';
}

require_once 'route.php';