<?php
$page = (isset($_GET['page']) ? $_GET['page']: '');

switch ($page) {
	case 'login':
		require_once __DIR__.'/user/module/login.php';
		break;
	
	default:
		require_once __DIR__.'/user/module/home.php';
		break;
}