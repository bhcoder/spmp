<?php
sessionAdmin();

getHeader();

$action = (isset($_GET['action']) ? $_GET['action'] : '');
$id = (isset($_GET['id']) ? $_GET['id'] : '');

switch ($action) {
	case 'tambah':
		if (isset($_POST['tambah'])) {
			$fpdo->insertInto('kerusakan')->values([
				'nm_kerusakan'=>$_POST['nm_kerusakan'],
				'solusi'=>$_POST['solusi']
				])->execute();
			header('location:index.php?page=kerusakan');
		}
		include "../admin/template/kerusakan/tambah.php";
		break;
	case 'ubah':
		if (isset($_POST['ubah'])) {
			$fpdo->update('kerusakan')->set([
				'nm_kerusakan'=>$_POST['nm_kerusakan'],
				'solusi'=>$_POST['solusi']
				])->where('kd_kerusakan', $id)->execute();
			header('location:index.php?page=kerusakan');
		}
		$tampilKers = $fpdo->from('kerusakan')->where('kd_kerusakan', $id);
		include "../admin/template/kerusakan/ubah.php";
		break;
	case 'hapus':
		if ($id) {
			$fpdo->deleteFrom('kerusakan')->where('kd_kerusakan', $id)->execute();
		}
		header('location:index.php?page=kerusakan');
		break;
	default:
		$tampilKers = $fpdo->from('kerusakan');
		require_once "../admin/template/kerusakan/list.php";
		break;
}

getFooter();