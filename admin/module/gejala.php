<?php
sessionAdmin();

getHeader();

$action = (isset($_GET['action']) ? $_GET['action'] : '');
$id = (isset($_GET['id']) ? $_GET['id'] : '');

switch ($action) {
	case 'tambah':
		if (isset($_POST['tambah'])) {
			$fpdo->insertInto('gejala')->values(['nm_gejala' => $_POST['nm_gejala']])->execute();
			header('location:index.php?page=gejala');
		}
		include "../admin/template/gejala/tambah.php";
		break;
	case 'ubah':
		if (isset($_POST['ubah'])) {
			$fpdo->update('gejala')->set('nm_gejala', $_POST['nm_gejala'])->where('kd_gejala', $id)->execute();
			header('location:index.php?page=gejala');
		}
		$tampilGejs = $fpdo->from('gejala')->where('kd_gejala', $id);
		include "../admin/template/gejala/ubah.php";
		break;
	case 'hapus':
		if ($id) {
			$fpdo->deleteFrom('gejala')->where('kd_gejala', $id)->execute();
		}
		header('location:index.php?page=gejala');
		break;
	default:
		$tampilGejs = $fpdo->from('gejala');
		require_once "../admin/template/gejala/list.php";
		break;
}

getFooter();