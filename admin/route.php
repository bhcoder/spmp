<?php
$page = (isset($_GET['page']) ? $_GET['page']: '');

if ($page == 'login') {
	require_once __DIR__.'/module/login.php';
}
elseif ($page =='sample') {
	require_once __DIR__.'/module/sample.php';
}
elseif ($page =='kerusakan') {
	require_once __DIR__.'/module/kerusakan.php';
}
elseif ($page =='gejala') {
	require_once __DIR__.'/module/gejala.php';
}
elseif ($page =='dashboard') {
	require_once __DIR__.'/module/dashboard.php';
}
else {
	require_once __DIR__.'/module/login.php';
}