<?php
ob_start();
session_start();
require_once '../lib/koneksi.php';

function getHeader() {
    require_once __DIR__.'/template/layout/header.php';
}

function getFooter() {
    require_once __DIR__.'/template/layout/footer.php';
}

function sessionAdmin() {
    if (!isset($_SESSION['id_admin'])) {
        header('location:index.php');
    }
}

require_once __DIR__.'/route.php';