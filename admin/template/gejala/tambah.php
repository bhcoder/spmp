<h2 class="mb-4">Tambah Data Gejala</h2>
<div class="card">
    <div class="card-body">
        <form method="POST">
            <div class="form-group">
                <label>Nama gejala</label>
                <input name="nm_gejala" type="text" class="form-control" placeholder="gejala">
            </div>
            <button name="tambah" type="submit" class="btn btn-primary">Submit</button>
            <a href="?page=gejala" class="btn btn-default">Cancel</a>
        </form>
    </div>
</div>