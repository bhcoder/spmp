<h2 class="mb-4">List Data Sample</h2>
<a href="?page=gejala&action=tambah" class="btn btn-dark mb-3">Tambah Data</a>
<table class="table table-admin">
  <thead class="bg-primary">
    <tr>
      <th scope="col">#</th>
      <th scope="col">gejala</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
  <?php foreach ($tampilGejs as $tampilGej) {?>
    <tr>
      <th scope="row"><?php echo "GO".$tampilGej['kd_gejala']; ?></th>
      <td><?php echo $tampilGej['nm_gejala']; ?></td>
      <td>
        <a href="?page=gejala&action=ubah&id=<?php echo $tampilGej['kd_gejala']; ?>" class="btn btn-warning">Edit</a>
        <a href="?page=gejala&action=hapus&id=<?php echo $tampilGej['kd_gejala']; ?>" class="btn btn-danger">Delete</a>
      </td>
  <?php } ?>
    </tr>
  </tbody>
</table>
