<h2 class="mb-4">Ubah Data gejala</h2>
<div class="card">
    <div class="card-body">
        <form method="POST">
            <div class="form-group">
                <label>Nama gejala</label>
                <input name="nm_gejala" type="text" class="form-control" value="<?php echo $tampilGejs->fetch('nm_gejala'); ?>">
            </div>
            <button name="ubah" type="submit" class="btn btn-primary">Ubah</button>
            <a href="?page=gejala" class="btn btn-default">Batal</a>
        </form>
    </div>
</div>