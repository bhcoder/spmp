<h2 class="mb-4">Tambah Data Kerusakan</h2>
<div class="card">
    <div class="card-body">
        <form method="POST">
            <div class="form-group">
                <label>Nama kerusakan</label>
                <input name="nm_kerusakan" type="text" class="form-control" placeholder="Kerusakan">
            </div>
            <div class="form-group">
                <label>Solusi</label>
                <textarea name="solusi" class="form-control" rows="3"></textarea>
            </div>

            <button name="tambah" type="submit" class="btn btn-primary">Submit</button>
            <a href="?page=kerusakan" class="btn btn-default">Cancel</a>
        </form>
    </div>
</div>