<h2 class="mb-4">List Data Sample</h2>
<a href="?page=kerusakan&action=tambah" class="btn btn-dark mb-3">Tambah Data</a>
<table class="table table-admin">
  <thead class="bg-primary">
    <tr>
      <th scope="col">#</th>
      <th scope="col">Kerusakan</th>
      <th scope="col">Solusi</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
  <?php foreach ($tampilKers as $tampilKer) {?>
    <tr>
      <th scope="row"><?php echo "KO".$tampilKer['kd_kerusakan']; ?></th>
      <td><?php echo $tampilKer['nm_kerusakan']; ?></td>
      <td><?php echo $tampilKer['solusi']; ?></td>
      <td>
        <a href="?page=kerusakan&action=ubah&id=<?php echo $tampilKer['kd_kerusakan']; ?>" class="btn btn-warning">Edit</a>
        <a href="?page=kerusakan&action=hapus&id=<?php echo $tampilKer['kd_kerusakan']; ?>" class="btn btn-danger">Delete</a>
      </td>
  <?php } ?>
    </tr>
  </tbody>
</table>
