<h2 class="mb-4">Ubah Data Kerusakan</h2>
<div class="card">
    <div class="card-body">
        <form method="POST">
            <div class="form-group">
                <label>Nama kerusakan</label>
                <input name="nm_kerusakan" type="text" class="form-control" value="<?php echo $tampilKers->fetch('nm_kerusakan'); ?>">
            </div>
            <div class="form-group">
                <label>Solusi</label>
                <textarea name="solusi" class="form-control" rows="3"><?php echo $tampilKers->fetch('solusi'); ?></textarea>
            </div>

            <button name="ubah" type="submit" class="btn btn-primary">Ubah</button>
            <a href="?page=kerusakan" class="btn btn-default">Batal</a>
        </form>
    </div>
</div>