<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>SPMB</title>
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/css/custom.css">
    <script src="../assets/js/jquery.min.js"></script>
    <script src="../assets/js/bootstrap.min.js"></script>
    <script src="../assets/js/custom.js"></script>
</head>
<body class="admin-body">

<div class="container">
    <div class="row justify-content-center">
        <div class="col-3">
            <img src="http://www.clker.com/cliparts/P/N/K/E/2/m/teal-car-md.png" alt="" width="40px" class="float-left mr-2">
            <p class="font-weight-bold mb-0">Administrator</p>
            <p class="mt-0" style="line-height: 0.6;">Hallo, Bugi</p>
            <hr style="width: 200px; margin: 10px 0;">
            <ul class="nav flex-column admin-menu">
                <li class="nav-item">
                    <a class="nav-link active" href="?page=gejala">Gejala</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="?page=kerusakan">Kerusakan</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="?page=aturan">Aturan</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="?page=diagnosa">Diagnosa</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="?page=user">User</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="?page=laporan">Laporan</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="?page=logout">Logout</a>
                </li>
            </ul>
        </div>
        <div class="col-7">